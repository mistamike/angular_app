import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { UserPageComponent } from './user-page/user-page.component';
import { AddNewPostComponent } from './add-new-post/add-new-post.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    UserPageComponent,
    AddNewPostComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
